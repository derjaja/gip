#include <stdio.h>
#include "string/my_string.h"

int main(){
  char s[1024];

  while(1){
    if(scanf("%s", s) != 1){
      break;
    }
    printf("Length of \"%s\": %d\n", s, my_strlen(s));
  }
  return 0;
}
